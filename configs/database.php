<?php

return [
    'local' => [
        'host'     => 'localhost',
        'user'     => 'root',
        'password' => 'server09',
        'database' => 'server_asteriskcdrdb',
        'table'    => 'cdr'
    ],

    'remote' => [
        'host'     => 'localhost',
        'user'     => 'root',
        'password' => 'server09',
        'database' => 'server_asteriskcdrdb',
        'table'    => 'calls',

        /* таблица куда дублируются записи */
        'double_table' => 'calls'
    ],
];
#!/usr/bin/php -q
<?php

namespace Asterisk;

use Asterisk\libraries\Db;
use mysqli;
use mysqli_result;
use \stdClass;

/* Подключение пакетов для отладки скрипта */
if (file_exists(__DIR__.'/vendor/autoload.php')) {
    include_once __DIR__.'/vendor/autoload.php';
    dump('loaded: /vendor/autoload.php');
}

/* Подключаем простой ActiveRecord для работы с БД */
require_once 'libraries/db.php';

/**
 * Класс переносит записи из CDR Asterisk'a
 * в базу данных CRM call_history
 */
class Asterisk
{
    /**
     * Подключение к локальной базе данных
     * на сервере с этим скриптом и Asterisk
     * @var mysqli
     */
    private $local;

    /**
     * Удаленное подключение к БД,
     * подключение к CRM
     * @var mysqli
     */
    private $remote;

    /**
     * Настройки подключения к БД
     * @var array
     */
    private $databases = [];

    /**
     * Настройки скрипта
     * @var array
     */
    private $configs = [];

    /**
     * @var string
     */
    private $uid;

    /**
     * Asterisk constructor.
     */
    public function __construct()
    {
        $this->configs   = include_once 'configs/migration.php';
        $this->databases = include_once 'configs/database.php';

        /* Подключение к локальной БД */
        $this->local = new Db($this->databases['local']);

        /* подключение к БД CRM */
        $this->remote = new Db($this->databases['remote']);

        if (empty($_SERVER['argv'][1])) {
            $this->error('Asterisk migration: отсутствуют аргументы с UID.');
            exit;
        }

        /* UID для логов */
        $this->uid = $_SERVER['argv'][1];

        /*  */
        $this->log('Инициализация скрипта');

        /* Выполним метод который указали в терминале */
        if (method_exists($this, $_SERVER['argv'][1])) {
            return $this->{$_SERVER['argv'][1]}();
        }

        /* Выполним миграцию звонков */
        $this->run();
    }

    /**
     * Получит записи и перекинет их в БД CRM
     */
    public function run()
    {
        /* получаем запись звонка из Asterisk */
        $call = $this->findCall($this->uid);
        if (! $call) {
            $this->error('Ошибка: не найдены звонки по UID.');
        }

        /* Формируем данные звонка */
        $callData = $this->getCallData($call);

        /* Переносим звонок в CRM, с новой структурой таблицы */
        if ($this->configs['use_beta_variant']) {
            return $this->migrationBeta($callData);
        }

        /* Переносим звонок в CRM со старой структорой */
        $this->migration($callData);

        /* если дублируем в новую таблицу данные */
        if ($this->configs['clone_beta_tables']) {
            $this->migrationBeta($callData);
        }

        $this->log("----- скрипт завершил работу -----\n");
        return true;
    }

    /**
     * Метод для тестирования и сбора данных.
     * Делает тоже самое что при обычной работе,
     * единственно не добавляет информацию в БД,
     * а выводит собранные данные в терминал.
     */
    public function test()
    {
        if (empty($_SERVER['argv'][2])) {
            dump('Не задан UID звонка.');
            return false;
        }

        /* получаем запись звонка из Asterisk */
        $call = $this->findCall($_SERVER['argv'][2]);
        if (! $call) {
            $this->error('Ошибка: не найдены звонки по UID.');
        }

        /* Формируем данные звонка */
        $callData = $this->getCallData($call);
        dump('--------- Данные из звонка -----------', $callData);

        /* Сформированные данные из звонка, те что идут в БД */
        dump('----------- Данные для БД -----------', $this->buildDataMigration($callData));
    }

    /**
     * Выводит последние записи в БД Asterisk'a
     */
    public function last()
    {
        $limit = ! empty($_SERVER['argv'][2]) ? intval($_SERVER['argv'][2]) : 10;
        if (! intval($limit)) $limit = 10;

        $this->local->select(['calldate', 'clid', 'src', 'dst', 'lastapp', 'uniqueid', 'duration', 'billsec']);
        $this->local->order_by('calldate', 'DESC');
        $this->local->limit($limit, 0);
        $calls = $this->local->get($this->databases['local']['table']);

        dump($calls);
    }

    /**
     * Вернет звонок для миграции
     * @param $unique_id
     * @return array
     */
    private function findCall($unique_id)
    {
        $this->log('Поиск записи в Asterisk CDR');

        $uid = substr($unique_id, 0, strpos($unique_id, '.'));

        // получим записи из cdr
        $this->local->like('uniqueid', $uid.'%');
        $this->local->where('src', 'Anonymous', '<>');
        $this->local->where('src', '', '<>');
        $this->local->where('dst', '', '<>');
        $this->local->where_not_in('lastapp', ['Zapateller', 'Busy']);
        $this->local->order_by('calldate', 'DESC');
        $this->local->limit(1, 0);

        $calls = $this->local->get($this->databases['local']['table']);

        $this->log('Найдено '. count($calls) .' записи в Asterisk CDR');

        /**
         * Если это груповой звонок, ищем тот на который ответили.
         */
        if (preg_match($this->configs['define_groups'], $calls[0]->dst)) {
            foreach ($calls as $call) {
                if ($this->isAnswered($call)) {
                    return $call;
                }
            }
        }

        return $calls ? $calls[0] : [];
    }

    /**
     * Сформирует данные звонка
     * @param $call
     * @return stdClass
     */
    private function getCallData($call)
    {
        if (! isset($call->duration)) $call->duration = 0;
        if (! isset($call->billsec))  $call->billsec = 0;

        $data = new stdClass();

        $data->uid           = $call->uniqueid;
        $data->direct        = $call->did;
        $data->direction     = $this->getDirection($call);
        $data->from          = $this->getNumber($call, 'src');
        $data->to            = $this->getNumber($call, 'dst');
        $data->start_time    = strtotime($call->calldate);
        $data->end_time      = $data->start_time + $call->duration;
        $data->duration      = intval($call->billsec);
        $data->answered      = $this->isAnswered($call);
        $data->answered_time = $data->answered ? $data->end_time - $call->billsec : null;
        $data->recording     = $call->billsec > 0 ? $call->recordingfile : '';
        $data->user_id       = $this->getUserId($data);
        $data->lead_id       = $this->getLeadId($data);
        $data->contact       = $this->getContact($data);
        $data->company_id    = $this->getCompanyId($data);

        $this->log($data, 'Сформированные данные звонка');

        return $data;
    }

    /**
     * Направление звонка
     * @param $call
     * @return string
     */
    private function getDirection($call)
    {
        return strlen($call->src) < strlen($call->dst) ? 'output' : 'input';
    }

    /**
     * Найдет и преобразует номер телефона в единый формат
     * @param $call
     * @param $type
     * @return string
     */
    private function getNumber($call, $type)
    {
        $number = $this->format($call->$type);

        if (strlen($number) < 4) {
            // поиск номера телефона из channel
            $key_channel = $type == 'src' ? 'channel' : 'dstchannel';
            $number = substr($call->$key_channel, strpos($call->$key_channel, '/') + 1, 4);
        }

        $this->log('Найден номер телефона: '. $number);

        return $number;
    }

    /**
     * Ответили на звонок или нет
     * @param $call
     * @return int
     */
    private function isAnswered($call)
    {
        return ($call->disposition === 'ANSWERED' && $call->lastapp !== 'Playback') ? 1 : 0;
    }

    /**
     * Найдет пользователя CRM по номеру телефона
     * @param $call
     * @return int
     */
    private function getUserId($call)
    {
        $this->remote->select(['id', 'user_name']);
        $this->remote->where_in('user_phone', [$call->from, $call->to]);

        $user = $this->remote->get('users', 1);

        if ($user) {
            $this->log('Найден пользователь: '. $user[0]->user_name);
        } else {
            $this->log('Пользователь не найден.');
        }

        return $user ? intval($user[0]->id) : 0;
    }

    /**
     * Найдет лид в CRM по номеру телефона
     * @param $call
     * @return int
     */
    private function getLeadId($call)
    {
        /* номер телефона клиента */
        $number = $this->getClientPhone($call);

        /**
         * Поиск ID лида когда номера хранятся в отдельной таблице
         */
        if ($this->configs['use_table_phones'])
        {
            $this->remote->select(['id', 'lead_id']);
            $this->remote->where($this->configs['phone_column_key'], $number);

            $phone_db = $this->remote->get($this->configs['use_table_phones'], 1);
            $lead_id = $phone_db ? $phone_db[0]->lead_id : null;
        } else {
            /**
             * Поиск лида по номера хранящихся у лидов
             */
            $_number = str_replace($this->configs['code_country'], '', $number);
            $this->remote->select(['id']);
            $this->remote->like('lead_phone', '%'.$_number);

            $lead_db = $this->remote->get($this->configs['use_table_phones'], 1);
            $lead_id = $lead_db ? $lead_db[0]->id : null;
        }

        /* если в настройках разрешено создать лид, создаем его */
        if (is_null($lead_id)) {
            $this->log('Лид по номеру телефона не найден');

            if ($this->configs['create_lead']) {
                $this->log('Создаем новый лид в CRM.');
                $lead_id = $this->createLead($call);
            }
        } else {
            $this->log('Найден лид по номеру телефона. ID: '. $lead_id);
        }

        return intval($lead_id);
    }

    /**
     * Поиск контакта
     * @param $call
     * @return array|mixed
     */
    private function getContact($call)
    {
        $contact = [];
        $number = $this->getClientPhone($call);

        if ($this->configs['use_table_phones'])
        {
            $this->remote->select(['lead_id']);
            if ($this->configs['format_phone']) {
                $this->remote->where($this->configs['phone_column_key'], $number);
            } else {
                $this->remote->like($this->configs['phone_column_key'], '%'.$number);
            }

            $phone_db = $this->remote->get($this->configs['use_table_phones'], 1);

            if ($phone_db) {
                $this->remote->select(['id', 'company_id', 'contact_name']);
                $this->remote->where('lead_id', $phone_db[0]->lead_id);
                $this->remote->where('system_status', 'active');
                $contact = $this->remote->get('contacts', 1);
            }
        }

        if (! $this->configs['use_table_phones'] || ! $contact) {
            $this->remote->select(['id', 'company_id', 'contact_name']);

            $this->remote->where('system_status', 'active');

            $keys = ['contact_phone', 'contact_phone2', 'contact_phone3', 'contact_phone4'];
            $this->remote->group_start();
            foreach ($keys as $index => $key) {
                if ($this->configs['format_phone']) {
                    $this->remote->or_where($key, $number);
                } else {
                    $_number = str_replace($this->configs['code_country'], '', $number);
                    $this->remote->or_like($key, '%'.$_number);
                }
            }
            $this->remote->group_end();

            $contact = $this->remote->get('contacts', 1);
        }

        if ($contact) {
            $this->log($contact, 'Найден контакт по номеру телефона');
        } else {
            $this->log('Контакт по номеру телефона не найден');
        }

        return $contact ? $contact[0] : null;
    }

    /**
     * Поиск ID компании
     * @param $call
     * @return int|null
     */
    private function getCompanyId($call)
    {
        if (! $call->contact || ! isset($call->contact->company_id)) {
            $this->log('ID компании не определен.');
            return null;
        }

        $this->log('Найдна компания контакта, ID: '. $call->contact->company_id);
        return $call->contact->company_id;
    }

    /**
     * Создаст новый лид и добавит ему номер телефона
     * @param $call
     * @return bool
     */
    private function createLead($call)
    {
        $data = array(
            'user_id'         => $call->user_id > 0 ? $call->user_id : -1,
            'date_created'    => date('Y-m-d H:i:s'),
            'lead_topic'      => '',
            'lead_name'       => 'Неизвестно',
            'lead_company'    => null,
            'lead_phone'      => $this->getClientPhone($call),
            'lead_address'    => '',
            'lead_mail'       => '',
            'lead_role'       => '',
            'lead_status'     => $call->user_id > 0 ? 1 : 2,
            'note'            => 'Лид создан автоматически по звонку.',
            'system_task'     => 0,
            'task_result'     => '',
            'system_status'   => 'active',
            'source'          => 'phone',
            'source_category' => ''
        );

        $this->remote->insert('leads', $data);
        $lead_id = $this->remote->insert_id();

        if (! $lead_id) {
            $this->log([$this->remote->errors(), $data], 'Не удалось создать лид.');
            return false;
        }

        $this->log('Создан новый лид. ID: '. $lead_id);

        // добавим номер телефона
        if ($this->configs['use_table_phones']) {
            $add_phone = $this->addPhoneNumber($call, $lead_id);

            if ($add_phone) {
                $this->log($call, 'Добавлен номер телефона');
            } else {
                $this->log([$this->remote->errors(), $call], 'Ошибка добавления номера телефона');
            }
        }

        return $lead_id;
    }

    /**
     * Добавит к лиду номер телефона
     * @param $call
     * @param $lead_id
     * @return bool|mysqli_result
     */
    private function addPhoneNumber($call, $lead_id)
    {
        $column = $this->configs['phone_column_key'];
        $number = $this->getClientPhone($call);

        $data = [
            'user_id'  => $call->user_id > 0 ? $call->user_id : -1,
            'lead_id'  => $lead_id,
            'type'     => 'phone',
            $column    => $this->format($number),
            'name'     => '',
            'default'  => 'Y',
            'modified' => date('Y-m-d H:i:s'),
            'created'  => date('Y-m-d H:i:s'),
        ];

        return $this->remote->insert($this->configs['use_table_phones'], $data);
    }

    /**
     * Сохранит телефонный звонок в БД CRM
     * @param $call
     * @return bool
     */
    private function migration($call)
    {
        $data = $this->buildDataMigration($call);
        $this->log($data, 'Подготовлен массив для переноса в CRM');

        // сохраним запись звонка если её еще нет
        if (! $this->callExists($call)) {
            $result = $this->remote->insert($this->databases['remote']['table'], $data);

            if ($result) {
                $call_id = $this->remote->insert_id();
                $this->log('Звонок перенесен в CRM. ID записи: '. $call_id);

                /* отметит предыдущие звонки как перезвонили */
                $this->checkedCallback($call, $call_id);

                return true;
            } else {
                $this->log([$this->remote->errors(), $data], 'Ошибка переноса звонка в CRM');
            }
        }

        return false;
    }


    /**
     * Создаст данные для добавления их в БД
     * @param $call
     */
    private function buildDataMigration($call)
    {
        return [
            'calldate'        => date('Y-m-d H:i:s', $call->start_time),
            'clid'            => $call->from,
            'uniqueid'        => $call->uid,
            'direction'       => $call->direction,
            'did'             => $call->direct,
            'contact_name'    => $call->contact ? $call->contact->contact_name : '',
            'dst'             => $call->to,
            'stat_user_ID'    => $call->user_id,
            'stat_name_user'  => (isset($user) && $user) ? $user[0]->user_name : '',
            'disposition'     => $call->answered ? 'ANSWERED' : 'NO ANSWER',
            'status_callback' => '',
            'connectstatus'   => '',
            'recordingfile'   => $call->recording,
            'lead_id'         => $call->lead_id,
            'stat_contact_id' => $call->contact ? intval($call->contact->id) : '',
            'stat_company_id' => $call->company_id,
        ];
    }


    /**
     * Проверит пропущенные звонки по номеру, если они есть
     * отметит их как "перезвонили".
     * @param $call
     * @param $callback_id
     * @return bool|mysqli_result
     */
    private function checkedCallback($call, $callback_id)
    {
        if (! $call->answered) {
            $this->log('Звонок не отвечен, пометка "Перезвонили" другим звонкам не требуются');
            return false;
        }

        $number = $this->getClientPhone($call);
        $number = str_replace($this->configs['code_country'], '', $number);

        // поиск пропущенных звонков от клиента
        $this->remote->where('calldate', date('Y-m-d H:i:s', $call->start_time), '<');
        $this->remote->like('clid', '%'.$number);
        $this->remote->where('direction', 'input');
        $this->remote->where('disposition', 'NO ANSWER');
        $this->remote->where('status_callback', '');
        $result = $this->remote->update($this->databases['remote']['table'], [
            'status_callback' => 1,
            'callback_id'     => $callback_id,
            'callback_date'   => date('Y-m-d H:i:s')
        ]);

        if ($result) {
            $this->log('Отметили зап. как "Перезвонили".');
        } else {
            $this->log($this->remote->errors(), 'Ошибка отметки "Перезвонили"');
        }

        return $result;
    }

    /**
     * Проверит есть ли уже запись с этим звонком
     * @param $call
     * @return bool
     */
    private function callExists($call)
    {
        $this->remote->where('uniqueid', $call->uid);
        $call = $this->remote->get($this->databases['remote']['table']);

        if ($call) {
            $this->log('Запись с таким UID уже есть в CRM.');
        } else {
            $this->log('Запись с таким UID уникальна в CRM.');
        }

        return $call ? true : false;
    }

    /**
     * Приведет номер телефона в единый формат
     * @param string $phone
     * @return bool
     */
    private function format($phone)
    {
        if ($this->configs['format_phone'] !== true) {
            return $phone;
        }

        $phone = trim(preg_replace('/[^0-9]/', '', $phone));

        if (empty($phone) || strlen($phone) < 4) {
            return $phone;
        }

        $code_country = preg_replace('/[^0-9]/', '', $this->configs['code_country']);
        if (strpos($phone, $code_country) === 0) {
            $phone = substr($phone, 2);
        }

        if (strlen($phone) == 9 && substr($phone, 0, 1) > 0) {
            $phone = '0'.$phone;
        }

        if (strlen($phone) >= 11 && substr($phone, 0, 2) == '00') {
            $phone = substr($phone, 1);
        }

        return $phone;
    }

    /**
     * Вернет номер телефона клиента
     * @param $call
     * @return mixed
     */
    private function getClientPhone($call)
    {
        return $call->direction == 'input' ? $call->from : $call->to;
    }

    /* ---------------------------------------------------------------------- */

    /**
     * Логирование работы скрипты
     * @param $text
     * @param $title
     * @param string $filename
     */
    private function log($text, $title = '', $filename = '')
    {
        if (is_array($text) || is_object($text)) {
            $text = json_encode($text);
        }

        if (! $this->configs['use_logs']) {
            dd('Логирование отключено в настройках.');
            return;
        }

        $folder = $this->configs['logs'];
        if (! file_exists($folder)) {
            mkdir($folder);
        }

        if (empty($filename)) {
            $filename = $this->uid .'.log';
        }

        $filename = $folder . $filename;

        // запишем заголовок
        if (! empty($title)) {
            file_put_contents($filename, date('Y-m-d H:i:s') .': '. $title ."\n", FILE_APPEND);
        }

        file_put_contents($filename, date('Y-m-d H:i:s') .': '. $text ."\n", FILE_APPEND);
    }

    /**
     * Запишет ошибку и остановит работу скрипта
     * @param $text
     * @param $title
     */
    private function error($text, $title = '')
    {
        $this->log($text, $title, $this->configs['errors']);
        $this->log("----- скрипт прерван с ошибкой -----\n", '', $this->configs['errors']);
        exit;
    }

    /* ---------------------------------------------------------------------- */

    /**
     * Сохранение копии звонка в новом формате.
     * @todo Тестовый метод, для удобного хранения звонков.
     * @param $call
     * @return bool
     */
    private function migrationBeta($call)
    {
        /* преобразуем в дату */
        foreach (['start_time', 'end_time', 'answered_time'] as $date_key) {
            if (! empty($call->$date_key)) {
                $call->$date_key = date('Y-m-d H:i:s', $call->$date_key);
            } else {
                $call->$date_key = null;
            }
        }

        /* получим полный путь к файлу записи */
        if (! empty($call->recording) && ! empty($call->start_time)) {
            $call->recording = '/'. date('Y/m/d', strtotime($call->start_time)) .'/'. $call->recording;
        }

        /* Удалим информацию о контакте и компании */
        unset($call->contact, $call->company_id);

        if (! $this->callExistsBeta($call)) {
            /* название таблицы куда пишем данные */
            $table = $this->configs['use_table_phones'];
            if (! $this->configs['use_beta_variant']) {
                $table = $this->databases['remote']['double_table'];
            }

            $result = $this->remote->insert($table, (array) $call);
            if ($result) {
                /* отметим пропущенные как перезвонили */
                $this->checkedCallbackBeta($call, $this->remote->insert_id());
            }

            return $result;
        }

        return false;
    }

    /**
     * Проверит пропущенные звонки по номеру, если они есть
     * отметит их как "перезвонили".
     * @param $call
     * @return bool|mysqli_result
     */
    private function checkedCallbackBeta($call, $call_id)
    {
        if (! $call->answered) {
            $this->log('Звонок не отвечен, пометка "Перезвонили" другим звонкам не требуются');
            return false;
        }

        $number = $this->getClientPhone($call);
        $number = str_replace($this->configs['code_country'], '', $number);

        // поиск пропущенных звонков от клиента
        $this->remote->where('start_time', date('Y-m-d H:i:s', $call->start_time), '<');
        $this->remote->like('from', '%'.$number);
        $this->remote->where('direction', 'input');
        $this->remote->where('answered', '0');
        $this->remote->where('callback', null, 'IS');

        /* название таблицы куда пишем данные */
        $table = $this->configs['use_table_phones'];
        if (! $this->configs['use_beta_variant']) {
            $table = $this->databases['remote']['double_table'];
        }

        return $this->remote->update($table, [
            'callback' => date('Y-m-d H:i:s'),
            'callback_id' => $call_id
        ]);
    }

    /**
     * Проверит записи в БД для тестового хранения
     * @param $call
     * @return bool
     */
    private function callExistsBeta($call)
    {
        /* название таблицы куда пишем данные */
        $table = $this->configs['use_table_phones'];
        if (! $this->configs['use_beta_variant']) {
            $table = $this->databases['remote']['double_table'];
        }

        $this->remote->where('uid', $call->uid);
        $call = $this->remote->get($table);
        return $call ? true : false;
    }
}

/* --------------------------------------------------------------------------- */
/**
 * Старт работы скрипта
 */
new Asterisk();
/* --------------------------------------------------------------------------- */

/**
 * Вывод информации из переменной
 */
if (! function_exists('dump')) {
    function dump($data) {
        var_dump($data);
    }
}

/**
 * Функция dump которая остановит работу скрипта
 */
if (! function_exists('dd')) {
    function dd($data) {
        dump($data);
        exit;
    }
}
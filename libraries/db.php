<?php

namespace Asterisk\libraries;

use mysqli;

class Db
{
    private $_table = '';

    private $_select = [];

    private $_where = [];

    private $_limit = null;

    private $_offset = null;

    private $_order_by = [];

    private $_query = '';

    private $ping = false;

    private $client = '';

    private $server = '';

    private $charset = '';

    private $collation = '';

    /**
     * @var mysqli
     */
    private $connect;


    public function __construct(array $config)
    {
        @$this->connect = $this->connection($config);
        if (! @$this->connect->ping()) {
            var_dump('Fail connected MySQL: ' . $config['database']);
            exit;
        }

        $this->connect->set_charset('utf8');

        $charset = $this->connect->get_charset();

        $this->client    = $this->connect->get_client_info();
        $this->server    = $this->connect->get_server_info();
        $this->ping      = $this->connect->ping();
        $this->charset   = $charset->charset;
        $this->collation = $charset->collation;
    }

    /**
     * @param string|null $table
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function get($table = null, $limit = null, $offset = null)
    {
        if (! empty($table)) {
            $this->from($table);
        }

        $this->limit($limit, $offset);
        $this->queryBuilderSelect();

        $result_db = $this->connect->query($this->_query);

        $results = [];
        if ($result_db) {
            while ($obj = $result_db->fetch_object()) {
                $results[] = $obj;
            }
        }

        /* сбросим последний запрос */
        $this->reset_query();

        return $results;
    }

    /**
     * @param string $table
     * @param array $data
     * @return bool|\mysqli_result
     */
    public function insert($table, array $data)
    {
        $this->from($table);

        $keys = [];
        foreach ($data as $key => $value) {
            $keys[] = $this->escape($key);
        }

        $values = $this->convert($data);

        $this->_query = implode("\n  ", [
            'INSERT INTO '. $this->_table,
            '('. implode(', ', $keys) .')',
            "VALUES\n    (". implode(", ", $values).')'
        ]);

        /* сбросим данные по запросу */
        $this->reset_query();

        return $this->connect->query($this->_query);
    }

    /**
     * @param string $table
     * @param array $data
     * @return bool|\mysqli_result
     */
    public function update($table, array $data)
    {
        if (! empty($table)) {
            $this->from($table);
        }

        $data = $this->convert($data);

        $sets = [];
        foreach ($data as $key => $value) {
            $sets[] = $key .' = '. $value;
        }

        $set_string = "SET \n    ". implode(",\n    ", $sets);

        $wheres = $this->getWhereQuery();
        if (! empty($wheres)) {
            $wheres = "\n  " . $wheres;
        }

        $this->_query = 'UPDATE '. $this->_table . "\n  " . $set_string . $wheres;

        /* сбросим данные по запросу */
        $this->reset_query();

        return $this->connect->query($this->_query);
    }

    /**
     * @param string $table
     * @param array $data
     * @return bool|\mysqli_result
     */
    public function delete($table, array $data)
    {
        if (!empty($table)) {
            $this->from($table);
        }

        foreach ($data as $key => $value) {
            $this->where($key, $value);
        }

        $this->_query = 'DELETE FROM '. $this->_table ."\n  ". $this->getWhereQuery();

        /* сбросим данные по запросу */
        $this->reset_query();

        return $this->connect->query($this->_query);
    }

    /**
     * @param string $table
     */
    public function from($table) {
        $this->_table = $table;
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     */
    public function limit($limit = null, $offset = null)
    {
        if (! $this->_limit) {
            $this->_limit = $limit;
        }

        if (! $this->_offset) {
            $this->_offset = $offset;
        }
    }

    /**
     * @return string
     */
    public function lastQuery()
    {
        return $this->_query;
    }

    /**
     * @return mixed
     */
    public function insert_id()
    {
        return $this->connect->insert_id;
    }

    /**
     * @return string
     */
    public function errors() {
        return mysqli_error($this->connect);
    }

    /**
     * Selected columns in database table
     * @param array $columns
     */
    public function select(array $columns = [])
    {
        $this->_select = array_merge($this->_select, $columns);
    }

    /**
     * @param string $key
     * @param string $value
     * @param string $operator
     */
    public function where($key, $value, $operator = '=')
    {
        $prefix = count($this->_where) ? 'AND ' : '';
        if ($this->detectStartGroup()) {
            $prefix = '';
        }

        $data = $this->convert([$key => $value]);

        $this->_where[] = implode(' ', [
            $prefix . $this->escape($key),
            $operator,
            $data[$key]
        ]);
    }

    /**
     * @param string $key
     * @param string $value
     * @param string $operator
     */
    public function or_where($key, $value, $operator = '=')
    {
        $prefix = count($this->_where) ? 'OR ' : '';
        if ($this->detectStartGroup()) {
            $prefix = '';
        }

        $data = $this->convert([$key => $value]);

        $this->_where[] = implode(' ', [
            $prefix . $this->escape($key),
            $operator,
            $data[$key]
        ]);
    }

    /**
     * @param string $key
     * @param array $values
     */
    public function where_in($key, array $values = [])
    {
        if (! is_array($values)) return;

        $prefix = count($this->_where) ? 'AND ' : '';
        if ($this->detectStartGroup()) {
            $prefix = '';
        }

        $values = $this->convert($values);

        $this->_where[] = implode(' ', [
            $prefix . $this->escape($key) .' IN',
            '('. implode(', ', $values) .')'
        ]);
    }

    /**
     * @param string $key
     * @param array $values
     */
    public function where_not_in($key, array $values = [])
    {
        if (! is_array($values)) return;

        $prefix = count($this->_where) ? 'AND ' : '';
        if ($this->detectStartGroup()) {
            $prefix = '';
        }

        $values = $this->convert($values);

        $this->_where[] = implode(' ', [
            $prefix . $this->escape($key) .' NOT IN',
            '('. implode(', ', $values) .')'
        ]);
    }

    public function like($key, $value)
    {
        $prefix = count($this->_where) ? 'AND ' : '';
        if ($this->detectStartGroup()) {
            $prefix = '';
        }

        $this->_where[] = implode(' ', [
            $prefix . $this->escape($key) . ' LIKE',
            "'".$value."'"
        ]);
    }

    public function or_like($key, $value)
    {
        $prefix = count($this->_where) ? 'OR ' : '';
        if ($this->detectStartGroup()) {
            $prefix = '';
        }

        $this->_where[] = implode(' ', [
            $prefix . $this->escape($key) . ' LIKE',
            "'".$value."'"
        ]);
    }

    public function group_start() {
        $prefix = count($this->_where) ? 'AND ' : '';
        $this->_where['group_'.mt_rand(0, 100000)] = $prefix . '(';
    }

    public function group_or_start() {
        $prefix = count($this->_where) ? 'OR ' : '';
        $this->_where['group_'.mt_rand(0, 100000)] = $prefix . '(';
    }

    public function group_end() {
        $this->_where['group_'.mt_rand(0, 100000)] = ')';
    }

    public function order_by($key, $direction = 'ASC')
    {
        $this->_order_by = [$key, $direction];
    }

    /**
     * Определяет является ли выборка началом группы
     * @return bool
     */
    private function detectStartGroup()
    {
        if (!count($this->_where)) {
            return false;
        }

        $wheres = array_slice($this->_where, -1);
        foreach ($wheres as $key => $value) {
            if (preg_match('/^group_/', $key)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $string
     * @return string
     */
    private function escape($string) {
        return '`'. trim($string) .'`';
    }

    /**
     * @param array $data
     * @return array
     */
    private function convert(array $data)
    {
        $values = [];
        foreach($data as $key => $value) {
            if (is_null($value)) {
                $values[$key] = 'NULL';
                continue;
            }

            if (is_numeric($value) && strlen($value) > 1 && substr($value, 0, 1) != 0) {
                $value = strpos($value, '.') ? floatval($value) : (int) $value;
            } else {
                $value = "'". trim($value) ."'";
            }

            $values[$key] = $value;
        }

        return $values;
    }

    /**
     *
     */
    private function queryBuilderSelect()
    {
        $order_by = '';
        if ($this->_order_by) {
            $order_by = 'ORDER BY '. $this->_order_by[0] .' '. $this->_order_by[1];
        }

        $this->_query = implode("\n", array_filter([
            $this->getSelectQuery(),
            'FROM '. $this->_table,
            $this->getWhereQuery(),
            $order_by,
            $this->getLimit(),
        ]));

        $this->_select = [];
        $this->_where = [];
        $this->_limit = null;
        $this->_offset = null;

        return $this->_query;
    }

    /**
     * @return string
     */
    private function getSelectQuery() {
        $select = array_filter($this->_select);
        $select_string = $select ? implode(', ', $select) : '*';
        return 'SELECT ' . $select_string;
    }

    /**
     * @return string
     */
    private function getWhereQuery()
    {
        if (! $this->_where) return '';

        $wheres = $this->_where;

        foreach ($wheres as $index => $where) {
            if (is_numeric($index)) {
                if (preg_match('/^(AND|OR)/', $where, $matches)) {
                    $operator = $matches[0];
                    $wheres[$index] = str_replace($operator . ' ', $operator . ' ' . $this->escape($this->_table) . '.', $where);
                } else {
                    $wheres[$index] = $this->escape($this->_table) . '.' . $where;
                }
            } else {
                $wheres[$index] = $where;
            }
        }

        return 'WHERE ' . implode("\n", $wheres);
    }

    /**
     * @return string
     */
    private function getLimit()
    {
        $result = '';
        if ($this->_limit && ! $this->_offset) {
            $result .= 'LIMIT '. $this->_limit;
        } elseif ($this->_limit && $this->_offset) {
            $result .= 'LIMIT '. $this->_offset .', '. $this->_limit;
        }

        return $result;
    }

    /**
     * Сбросит значения последнего запроса
     */
    private function reset_query()
    {
        $this->_select = [];
        $this->_where = [];
        $this->_limit = null;
        $this->_offset = null;
        $this->_order_by = [];
    }

    /**
     * @param array $config
     * @return mysqli
     */
    private function connection(array $config)
    {
        return new mysqli(
            $config['host'],
            $config['user'],
            $config['password'],
            $config['database']);
    }
}
